# Chat-Room
This application was created using php and ajax. One can login to the chat room and chat if they are connected to the same network. This Applicaiton is deployed on Azure Cloud Services


# Conditions to use this applicaiton
- All the devices should be connected to the same network and must have a working internet connection.
- Laptops and pc are most preferable to use this applicaiton.
- This chat-room is a public chat-room (i.e. It doesn't provide the provision of personal chats)

# Steps to use the applicaiton
- One must first login to the chat-room using a username.
- After login one can chat with other connected devices.
- One can exit the chatroom by clicking exit chat link.

